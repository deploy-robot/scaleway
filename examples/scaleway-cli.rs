use scaleway_rs::{ScalewayApi, ScalewayError};
use std::env;

fn main() {
    let args: Vec<String> = env::args().collect();
    if args.len() != 2 {
        println!("Call program with the following:");
        println!("{} API_KEY DOMAIN", args[0]);
        std::process::exit(1);
    }

    let result = do_stuff(&args[1]);
    match result {
        Ok(_) => {
            println!("Finished sucessfully");
        }
        Err(e) => {
            println!("Error: {}", e);
        }
    }
}

fn do_stuff(api_key: &str) -> Result<(), ScalewayError> {
    let region = "fr-par-2";
    let api = ScalewayApi::new(api_key);

    let types = api.get_server_types()?;
    println!("SERVERTYPES: {:#?}", types);

    let images = api.list_images(region).run()?;
    println!("IMAGES: {:#?}", images);

    let instances = api.list_instances(region).order("creation_date_asc").run()?;
    println!("INSTANCES: {:#?}", instances);

    let availability = api.list_availability()?;
    println!("AVAILABILITY: {:#?}", availability);

    let list = api
        .list_marketplace_instances()
        .arch("x86_64")
        .include_eol(false)
        .run()?;
    println!("MARKETPLACE IMAGES: {:#?}", list);

    let list = api.list_marketplace_instance_versions(&list[0].id).run()?;
    println!("MARKETPLACE IMAGE VERSION: {:#?}", list);

    Ok(())
}
