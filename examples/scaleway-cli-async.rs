use scaleway_rs::{LocalImageListType, ScalewayApi, ScalewayError};
use std::env;

#[async_std::main]
async fn main() -> Result<(), ScalewayError> {
    let args: Vec<String> = env::args().collect();
    if args.len() != 2 {
        println!("Call program with the following:");
        println!("{} SCALEWAY_API_KEY", args[0]);
        std::process::exit(1);
    }

    let result = do_stuff(&args[1]).await;
    match result {
        Ok(_) => {
            println!("Finished sucessfully");
        }
        Err(e) => {
            println!("Error: {}", e);
        }
    }
    Ok(())
}

async fn do_stuff(api_key: &str) -> Result<(), ScalewayError> {
    let region = "fr-par-2";
    let api = ScalewayApi::new(api_key);

    let types = api.get_server_types_async(region).await?;
    println!("SERVERTYPES: {:#?}", types);

    let images = api.list_images(region).run_async().await?;
    println!("IMAGES: {:#?}", images);

    let instances = api
        .list_instances(region)
        .order("creation_date_asc")
        .run_async()
        .await?;
    println!("INSTANCES: {:#?}", instances);

    let availability = api.list_availability_async(region).await?;
    println!("AVAILABILITY: {:#?}", availability);

    let list_marketplace_images = api
        .list_marketplace_instances()
        .arch("x86_64")
        .include_eol(false)
        .run_async()
        .await?;
    println!("MARKETPLACE IMAGES: {:#?}", list_marketplace_images);

    let list_image_version = api
        .list_marketplace_instance_versions(&list_marketplace_images[0].id)
        .run_async()
        .await?;
    println!("MARKETPLACE IMAGE VERSION: {:#?}", list_image_version);

    let list_local_images = api
        .list_marketplace_local_images(LocalImageListType::ByImageId(
            list_marketplace_images[0].id.to_string(),
        ))
        .run_async()
        .await?;
    println!("MARKETPLACE LOCAL IMAGES: {:#?}", list_local_images);

    Ok(())
}
