# scaleway-rs

A pure Rust Scaleway API binding.

## Examples

### Example blocking

It needs to have the feature "blocking" enabled.

```toml
scaleway-rs = { version = "*", features = ["blocking"] }
```

```rust
use scaleway_rs::ScalewayApi;
use scaleway_rs::ScalewayError;

fn main() -> Result<(), ScalewayError> {
    let region = "fr-par-2";
    let api = ScalewayApi::new("<KEY>");

    let types = api.get_server_types_async(region).await?;
    println!("SERVERTYPES: {:#?}", types);

    let images = api.list_images(region).run_async().await?;
    println!("IMAGES: {:#?}", images);

    let instances = api
        .list_instances(region)
        .order("creation_date_asc")
        .run_async()
        .await?;
    println!("INSTANCES: {:#?}", instances);
    Ok(())
}
```

### Example async

```toml
scaleway-rs = { version = "*" }
```

```rust
use scaleway_rs::ScalewayApi;
use scaleway_rs::ScalewayError;

#[async_std::main]
async fn main() -> Result<(), ScalewayError> {
    let region = "fr-par-2";
    let api = ScalewayApi::new("<KEY>");

    let types = api.get_server_types(region)?;
    println!("SERVERTYPES: {:#?}", types);

    let images = api.list_images(region).run()?;
    println!("IMAGES: {:#?}", images);

    let instances = api.list_instances(region).order("creation_date_asc").run()?;
    println!("INSTANCES: {:#?}", instances);
    Ok(())
}
```

## Features

* "default" - use nativetls
* "default-rustls" - use rusttls
* "blocking" - enable blocking api
* "rustls" - enable rustls for reqwest
* "nativetls" - add support for nativetls DEFAULT
* "gzip" - enable gzip in reqwest
* "brotli" - enable brotli in reqwest
* "deflate" - enable deflate in reqwest

## TODO

* [ ] Documentation
* [ ] Full api support
