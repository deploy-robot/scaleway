use serde::Deserialize;

#[derive(Deserialize, Debug)]
pub struct ScalewayApiError {
    pub message: String,
    pub resource: Option<String>,
    pub resource_id: Option<String>,
    #[serde(rename = "type")]
    pub etype: String,
}
