use std::collections::HashMap;
use serde::Deserialize;

#[derive(Deserialize, Debug)]
pub struct ScalewayImageRoot {
    pub images: Vec<ScalewayImage>,
}

#[derive(Deserialize, Debug)]
pub struct ScalewayImage {
    pub id: String,
    pub name: String,
    pub arch: String,
    pub creation_date: String,
    pub modification_date: String,
    pub from_server: Option<String>,
    pub organization: String,
    pub public: bool,
    pub state: String,
    pub project: String,
    pub tags: Vec<String>,
    pub zone: String,
    pub root_volume: ScalewayImageRootVolume,
    pub default_bootscript: Option<ScalewayImageBootscript>,
    pub extra_volumes: ScalewayImageExtraVolumes,
}

#[derive(Deserialize, Debug)]
pub struct ScalewayImageRootVolume {
    pub id: String,
    pub name: String,
    pub size: u64,
    pub volume_type: String,
}

#[derive(Deserialize, Debug)]
pub struct ScalewayImageBootscript {
    pub bootcmdargs: String,
    pub default: bool,
    pub dtb: String,
    pub id: String,
    pub initrd: String,
    pub kernel: String,
    pub organization: String,
    pub project: String,
    pub public: bool,
    pub title: String,
    pub arch: Option<String>,
    pub zone: String,
}

#[derive(Deserialize, Debug)]
pub struct ScalewayImageExtraVolumes {
    #[serde(flatten)]
    pub volumes: HashMap<String, ScalewayImageExtraVolume>,
}

#[derive(Deserialize, Debug)]
pub struct ScalewayImageExtraVolume {
    pub id: String,
    pub name: String,
    pub export_uri: Option<String>,
    pub size: u64,
    pub volume_type: String,
    pub creation_date: String,
    pub modification_date: String,
    pub organization: String,
    pub project: String,
    pub tags: Vec<String>,
    pub server: ScalewayImageExtraVolumeServer,
    pub state: String,
    pub zone: String,
}

#[derive(Deserialize, Debug)]
pub struct ScalewayImageExtraVolumeServer {
    pub id: String,
    pub name: String,
}
