use serde::Deserialize;

#[derive(Deserialize, Debug)]
pub struct ScalewayUserDataKeyList {
    pub user_data: Vec<String>,
}


#[derive(Deserialize, Debug)]
pub struct ScalewayUserData {
    pub name: String,
    pub content_type: String,
    pub content: String,
}
