use serde::Deserialize;

#[derive(Deserialize, Debug)]
pub struct ScalewayMarketplaceLocalImagesRoot {
    pub local_images: Vec<ScalewayMarketplaceLocalImage>,
    pub total_count: u64,
}

#[derive(Deserialize, Debug)]
pub struct ScalewayMarketplaceLocalImage {
    pub id: String,
    pub compatible_commercial_types: Vec<String>,
    pub arch: String,
    pub zone: String,
    pub label: String,
    #[serde(rename = "type")]
    pub itype: String,
}
