use serde::Deserialize;

#[derive(Deserialize, Debug)]
pub struct ScalewayMarketplaceImageVersionRoot {
    pub versions: Vec<ScalewayMarketplaceImageVersion>,
    pub total_count: u64,
}

#[derive(Deserialize, Debug)]
pub struct ScalewayMarketplaceImageVersion {
    pub id: String,
    pub name: String,
    pub created_at: String,
    pub updated_at: String,
    pub published_at: Option<String>,
}
