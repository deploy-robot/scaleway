use std::collections::HashMap;

use serde::Deserialize;

#[derive(Deserialize, Debug)]
pub struct ScalewayServerTypeRoot {
    pub servers: ScalewayServerTypeItem,
}

#[derive(Deserialize, Debug)]
pub struct ScalewayServerTypeItem {
    #[serde(flatten)]
    pub servers: HashMap<String, ScalewayServerType>,
}

#[derive(Deserialize, Debug)]
pub struct ScalewayServerType {
    pub alt_names: Vec<String>,
    pub arch: String,
    pub ncpus: u32,
    pub ram: u64,
    pub gpu: u32,
    pub baremetal: bool,
    pub monthly_price: f32,
    pub hourly_price: f32,
    pub network: ScalewayServerTypeNetwork,
}

#[derive(Deserialize, Debug)]
pub struct ScalewayServerTypeNetwork {
    pub ipv6_support: bool,
    pub sum_internal_bandwidth: u64,
    pub sum_internet_bandwidth: u64,
}

#[derive(Deserialize, Debug)]
pub struct ServerType {
    pub id: String,
    pub location: String,
    
    pub alt_names: Vec<String>,
    pub arch: String,
    pub ncpus: u32,
    pub ram: u64,
    pub gpu: u32,
    pub baremetal: bool,
    pub monthly_price: f32,
    pub hourly_price: f32,
    pub network: ScalewayServerTypeNetwork,
}