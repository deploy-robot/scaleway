use serde::Deserialize;

use super::image::{ScalewayImage, ScalewayImageBootscript, ScalewayImageExtraVolumes};

#[derive(Deserialize, Debug)]
pub struct ScalewayInstancesRoot {
    pub servers: Vec<ScalewayInstance>,
}

#[derive(Deserialize, Debug)]
pub struct ScalewayInstanceRoot {
    pub server: ScalewayInstance,
}

#[derive(Deserialize, Debug)]
pub struct ScalewayInstance {
    pub id: String,
    pub name: String,
    pub organization: String,
    pub project: String,
    pub allowed_actions: Vec<String>,
    pub tags: Vec<String>,
    pub commercial_type: String,
    pub creation_date: Option<String>,
    pub dynamic_ip_required: bool,
    pub routed_ip_enabled: bool,
    pub enable_ipv6: bool,
    pub hostname: String,
    pub image: ScalewayImage,
    pub protected: bool,
    pub private_ip: Option<String>,
    pub public_ip: Option<ScalewayPublicIP>,
    pub public_ips: Vec<ScalewayPublicIP>,
    pub mac_address: String,
    pub modification_date: Option<String>,
    pub state: String,
    pub location: Option<ScalewayInstanceLocation>,
    pub ipv6: Option<ScalewayIpv6>,
    pub bootscript: ScalewayImageBootscript,
    pub boot_type: String,
    pub volumes: ScalewayImageExtraVolumes,
    pub security_group: ScalewaySecurityGroup,
    pub maintenances: Vec<ScalewayMaintenance>,
    pub state_detail: String,
    pub arch: String,
    pub placement_group: Option<ScalewayPlacementGroup>,
    pub private_nics: Vec<ScalewayPrivateNic>,
    pub zone: String,
}

#[derive(Deserialize, Debug)]
pub struct ScalewayPublicIP {
    pub id: String,
    pub address: String,
    pub gateway: Option<String>,
    pub netmask: String,
    pub family: String,
    pub dynamic: bool,
    pub provisioning_mode: String,
    pub tags: Vec<String>,
    pub state: String,
}

#[derive(Deserialize, Debug)]
pub struct ScalewayInstanceLocation {
    pub cluster_id: String,
    pub hypervisor_id: String,
    pub node_id: String,
    pub platform_id: String,
    pub zone_id: String,
}

#[derive(Deserialize, Debug)]
pub struct ScalewayIpv6 {
    pub address: String,
    pub gateway: Option<String>,
    pub netmask: String,
}

#[derive(Deserialize, Debug)]
pub struct ScalewaySecurityGroup {
    pub id: String,
    pub name: String,
}

#[derive(Deserialize, Debug)]
pub struct ScalewayMaintenance {
    pub reason: String,
}

#[derive(Deserialize, Debug)]
pub struct ScalewayPlacementGroup {
    pub id: String,
    pub name: String,
    pub organization: String,
    pub project: String,
    pub tags: Vec<String>,
    pub policy_mode: String,
    pub policy_type: String,
    pub policy_respected: bool,
    pub zone: String,
}

#[derive(Deserialize, Debug)]
pub struct ScalewayPrivateNic {
    pub id: String,
    pub server_id: String,
    pub private_network_id: String,
    pub mac_address: String,
    pub state: String,
    pub tags: Vec<String>,
}
