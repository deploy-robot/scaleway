use serde::Deserialize;

#[derive(Deserialize, Debug)]
pub struct ScalewayMarketplaceImageRoot
{
    pub images: Vec<ScalewayMarketplaceImage>,
    pub total_count: u64,
}

#[derive(Deserialize, Debug)]
pub struct ScalewayMarketplaceImage
{
    pub id: String,
    pub name: String,
    pub description: String,
    pub logo: String,
    pub categories: Vec<String>,
    pub created_at: String,
    pub updated_at: Option<String>,
    pub valid_until: Option<String>,
    pub label: String,
}