use std::collections::HashMap;
use serde::Deserialize;

#[derive(Deserialize, Debug)]
pub struct ScalewayAvailabilityRoot {
    pub servers: ScalewayAvailabilityList,
}

#[derive(Deserialize, Debug)]
pub struct ScalewayAvailabilityList {
    #[serde(flatten)]
    pub servers: HashMap<String, ScalewayAvailability>,
}

#[derive(Deserialize, Debug)]
pub struct ScalewayAvailability {
    pub availability: String,
}
