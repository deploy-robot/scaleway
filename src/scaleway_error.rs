use std::fmt::Display;
use crate::api_error::ScalewayApiError;

#[derive(Debug)]
pub enum ScalewayError {
    Reqwest(reqwest::Error),
    Api(ScalewayApiError),
    Json(),
}

impl From<reqwest::Error> for ScalewayError {
    fn from(value: reqwest::Error) -> Self {
        ScalewayError::Reqwest(value)
    }
}

impl Display for ScalewayError {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::result::Result<(), std::fmt::Error> {
        match self {
            ScalewayError::Reqwest(err) => write!(f, "Request: {}", err),
            ScalewayError::Api(err) => write!(f, "Api: {} - {}", err.etype, err.message),
            ScalewayError::Json() => write!(f, "JSON"),
        }
    }
}

impl std::error::Error for ScalewayError {
    fn source(&self) -> Option<&(dyn std::error::Error + 'static)> {
        None
    }

    fn cause(&self) -> Option<&dyn std::error::Error> {
        self.source()
    }
}