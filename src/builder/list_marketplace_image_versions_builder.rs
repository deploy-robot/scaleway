use crate::{
    data::marketplace_image_version::{
        ScalewayMarketplaceImageVersion, ScalewayMarketplaceImageVersionRoot,
    },
    ScalewayApi, ScalewayError,
};

pub struct ScalewayListMarketplaceImageVersionsBuilder {
    api: ScalewayApi,
    params: Vec<(&'static str, String)>,
}

impl ScalewayListMarketplaceImageVersionsBuilder {
    pub fn new(api: ScalewayApi, image_id: &str) -> Self {
        ScalewayListMarketplaceImageVersionsBuilder {
            api,
            params: vec![("image_id", image_id.to_string())],
        }
    }

    /// Reference: https://www.scaleway.com/en/developers/api/marketplace/#path-marketplace-image-versions-list-versions-of-an-image
    ///
    /// Possible Values: created_at_asc, created_at_desc
    pub fn order_by(mut self, order_by: &str) -> ScalewayListMarketplaceImageVersionsBuilder {
        self.params.push(("order_by", order_by.to_string()));
        self
    }

    #[cfg(feature = "blocking")]
    pub fn run(self) -> Result<Vec<ScalewayMarketplaceImageVersion>, ScalewayError> {
        let mut list = vec![];
        let mut page = 1;
        loop {
            let mut params = self.params.clone();
            params.push(("page", page.to_string()));
            let result = self
                .api
                .get("https://api.scaleway.com/marketplace/v2/versions", params)?
                .json::<ScalewayMarketplaceImageVersionRoot>()?;
            if result.versions.len() == 0 {
                break;
            }
            list.extend(result.versions.into_iter());
            if list.len() == (result.total_count as usize) {
                break;
            }
            page += 1;
        }
        Ok(list)
    }

    pub async fn run_async(self) -> Result<Vec<ScalewayMarketplaceImageVersion>, ScalewayError> {
        let mut list = vec![];
        let mut page = 1;
        loop {
            let mut params = self.params.clone();
            params.push(("page", page.to_string()));
            let result = self
                .api
                .get_async("https://api.scaleway.com/marketplace/v2/versions", params)
                .await?
                .json::<ScalewayMarketplaceImageVersionRoot>()
                .await?;
            if result.versions.len() == 0 {
                break;
            }
            list.extend(result.versions.into_iter());
            if list.len() == (result.total_count as usize) {
                break;
            }
            page += 1;
        }
        Ok(list)
    }
}
