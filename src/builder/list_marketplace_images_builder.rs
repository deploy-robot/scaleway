use crate::{
    data::marketplace_image::{ScalewayMarketplaceImage, ScalewayMarketplaceImageRoot},
    ScalewayApi, ScalewayError,
};

pub struct ScalewayListMarketplaceImagesBuilder {
    api: ScalewayApi,
    params: Vec<(&'static str, String)>,
}

impl ScalewayListMarketplaceImagesBuilder {
    pub fn new(api: ScalewayApi) -> Self {
        ScalewayListMarketplaceImagesBuilder {
            api,
            params: vec![],
        }
    }

    /// Reference: https://www.scaleway.com/en/developers/api/marketplace/#path-marketplace-images-list-marketplace-images
    ///
    /// Possible Values: name_asc, name_desc, created_at_asc, created_at_desc, updated_at_asc, updated_at_desc
    pub fn order_by(mut self, order_by: &str) -> ScalewayListMarketplaceImagesBuilder {
        self.params.push(("order_by", order_by.to_string()));
        self
    }

    pub fn arch(mut self, arch: &str) -> ScalewayListMarketplaceImagesBuilder {
        self.params.push(("arch", arch.to_string()));
        self
    }

    pub fn category(mut self, category: &str) -> ScalewayListMarketplaceImagesBuilder {
        self.params.push(("category", category.to_string()));
        self
    }

    pub fn include_eol(mut self, include_eol: bool) -> ScalewayListMarketplaceImagesBuilder {
        self.params.push(("include_eol", include_eol.to_string()));
        self
    }

    #[cfg(feature = "blocking")]
    pub fn run(self) -> Result<Vec<ScalewayMarketplaceImage>, ScalewayError> {
        let mut list = vec![];
        let mut page = 1;
        loop {
            let mut params = self.params.clone();
            params.push(("page", page.to_string()));
            let result = self
                .api
                .get("https://api.scaleway.com/marketplace/v2/images", params)?
                .json::<ScalewayMarketplaceImageRoot>()?;
            if result.images.len() == 0 {
                break;
            }
            list.extend(result.images.into_iter());
            if list.len() == (result.total_count as usize) {
                break;
            }
            page += 1;
        }
        Ok(list)
    }

    pub async fn run_async(self) -> Result<Vec<ScalewayMarketplaceImage>, ScalewayError> {
        let mut list = vec![];
        let mut page = 1;
        loop {
            let mut params = self.params.clone();
            params.push(("page", page.to_string()));
            let result = self
                .api
                .get_async("https://api.scaleway.com/marketplace/v2/images", params)
                .await?
                .json::<ScalewayMarketplaceImageRoot>()
                .await?;
            if result.images.len() == 0 {
                break;
            }
            list.extend(result.images.into_iter());
            if list.len() == (result.total_count as usize) {
                break;
            }
            page += 1;
        }
        Ok(list)
    }
}
