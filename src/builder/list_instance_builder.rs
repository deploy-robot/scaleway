use crate::{
    data::instance::{ScalewayInstance, ScalewayInstancesRoot},
    ScalewayApi, ScalewayError,
};

pub struct ScalewayListInstanceBuilder {
    api: ScalewayApi,
    zone: String,
    params: Vec<(&'static str, String)>,
}

impl ScalewayListInstanceBuilder {
    pub fn new(api: ScalewayApi, zone: &str) -> Self {
        ScalewayListInstanceBuilder {
            api,
            zone: zone.to_string(),
            params: vec![],
        }
    }

    /// A positive integer lower or equal to 100 to select the number of items to return.
    pub fn per_page(mut self, count: u32) -> ScalewayListInstanceBuilder {
        self.params.push(("per_page", count.to_string()));
        self
    }

    /// A positive integer to choose the page to return.
    pub fn page(mut self, count: u32) -> ScalewayListInstanceBuilder {
        self.params.push(("page", count.to_string()));
        self
    }

    /// List only Instances of this Organization ID.
    pub fn organization(mut self, organization: &str) -> ScalewayListInstanceBuilder {
        self.params.push(("organization", organization.to_string()));
        self
    }

    /// List only Instances of this Project ID.
    pub fn project(mut self, project: &str) -> ScalewayListInstanceBuilder {
        self.params.push(("project", project.to_string()));
        self
    }

    /// Filter Instances by name (eg. "server1" will return "server100" and "server1" but not "foo").
    pub fn name(mut self, name: &str) -> ScalewayListInstanceBuilder {
        self.params.push(("name", name.to_string()));
        self
    }

    /// List Instances by private_ip. (IP address)
    pub fn private_ip(mut self, private_ip: &str) -> ScalewayListInstanceBuilder {
        self.params.push(("private_ip", private_ip.to_string()));
        self
    }

    /// List Instances that are not attached to a public IP.
    pub fn without_ip(mut self, without_ip: bool) -> ScalewayListInstanceBuilder {
        self.params.push(("without_ip", without_ip.to_string()));
        self
    }

    /// List Instances of this commercial type.
    pub fn commercial_type(mut self, commercial_type: &str) -> ScalewayListInstanceBuilder {
        self.params
            .push(("commercial_type", commercial_type.to_string()));
        self
    }

    /// List Instances in this state.
    pub fn state(mut self, state: &str) -> ScalewayListInstanceBuilder {
        self.params.push(("state", state.to_string()));
        self
    }

    /// List Instances with these exact tags (to filter with several tags, use commas to separate them).
    pub fn tags(mut self, tags: &str) -> ScalewayListInstanceBuilder {
        self.params.push(("tags", tags.to_string()));
        self
    }

    /// List Instances in this Private Network.
    pub fn private_network(mut self, private_network: &str) -> ScalewayListInstanceBuilder {
        self.params
            .push(("private_network", private_network.to_string()));
        self
    }

    /// Define the order of the returned servers.
    /// Default: creation_date_desc
    /// Possible values: creation_date_desc, creation_date_asc, modification_date_desc, modification_date_asc
    pub fn order(mut self, order: &str) -> ScalewayListInstanceBuilder {
        self.params.push(("order", order.to_string()));
        self
    }

    /// List Instances from the given Private Networks (use commas to separate them).
    pub fn private_networks(mut self, private_networks: &str) -> ScalewayListInstanceBuilder {
        self.params
            .push(("private_networks", private_networks.to_string()));
        self
    }

    /// List Instances associated with the given private NIC MAC address.
    pub fn private_nic_mac_address(
        mut self,
        private_nic_mac_address: &str,
    ) -> ScalewayListInstanceBuilder {
        self.params.push((
            "private_nic_mac_address",
            private_nic_mac_address.to_string(),
        ));
        self
    }

    /// List Instances from these server ids (use commas to separate them).
    pub fn servers(mut self, servers: &str) -> ScalewayListInstanceBuilder {
        self.params.push(("servers", servers.to_string()));
        self
    }

    #[cfg(feature = "blocking")]
    pub fn run(self) -> Result<Vec<ScalewayInstance>, ScalewayError> {
        let mut list = vec![];
        let mut page = 1;
        loop {
            let url = &format!(
                "https://api.scaleway.com/instance/v1/zones/{zone}/servers",
                zone = self.zone
            );
            let mut params = self.params.clone();
            params.push(("page", page.to_string()));
            let result = self
                .api
                .get(&url, params)?
                .json::<ScalewayInstancesRoot>()?;
            if result.servers.len() == 0 {
                break;
            }
            list.extend(result.servers.into_iter());
            page += 1;
        }
        Ok(list)
    }

    pub async fn run_async(self) -> Result<Vec<ScalewayInstance>, ScalewayError> {
        let mut list = vec![];
        let mut page = 1;
        loop {
            let url = &format!(
                "https://api.scaleway.com/instance/v1/zones/{zone}/servers",
                zone = self.zone
            );
            let mut params = self.params.clone();
            params.push(("page", page.to_string()));
            let result = self
                .api
                .get_async(&url, params)
                .await?
                .json::<ScalewayInstancesRoot>()
                .await?;
            if result.servers.len() == 0 {
                break;
            }
            list.extend(result.servers.into_iter());
            page += 1;
        }
        Ok(list)
    }
}
