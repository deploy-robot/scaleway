use crate::{data::instance::ScalewayInstanceRoot, ScalewayApi, ScalewayError, ScalewayInstance};
use serde::Serialize;

pub struct ScalewayCreateInstanceBuilder {
    api: ScalewayApi,
    zone: String,
    config: CreateInstanceConfig,
}

#[derive(Serialize, Debug)]
struct CreateInstanceConfig {
    name: String,
    commercial_type: String,
    #[serde(skip_serializing_if = "Option::is_none")]
    routed_ip_enabled: Option<bool>,
    #[serde(skip_serializing_if = "Option::is_none")]
    image: Option<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    boot_type: Option<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    bootscript: Option<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    project: Option<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    public_ip: Option<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    enable_ipv6: Option<bool>,
    #[serde(skip_serializing_if = "Option::is_none")]
    tags: Option<Vec<String>>,
    #[serde(skip_serializing_if = "Option::is_none")]
    security_group: Option<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    placement_group: Option<String>,
}

impl ScalewayCreateInstanceBuilder {
    pub fn new(api: ScalewayApi, zone: &str, name: &str, commercial_type: &str) -> Self {
        ScalewayCreateInstanceBuilder {
            api,
            zone: zone.to_string(),
            config: CreateInstanceConfig {
                name: name.to_string(),
                commercial_type: commercial_type.to_string(),
                routed_ip_enabled: None,
                image: None,
                boot_type: None,
                bootscript: None,
                enable_ipv6: None,
                public_ip: None,
                project: None,
                tags: None,
                security_group: None,
                placement_group: None,
            },
        }
    }

    /// A positive integer lower or equal to 100 to select the number of items to return.
    pub fn dynamic_ip_required(mut self, b: bool) -> ScalewayCreateInstanceBuilder {
        self.config.routed_ip_enabled = Some(b);
        self
    }

    /// If true, configure the Instance so it uses the new routed IP mode.
    pub fn routed_ip_enabled(mut self, routed_ip_enabled: bool) -> ScalewayCreateInstanceBuilder {
        self.config.routed_ip_enabled = Some(routed_ip_enabled);
        self
    }

    /// Instance image ID.
    pub fn image(mut self, image: &str) -> ScalewayCreateInstanceBuilder {
        self.config.image = Some(image.to_string());
        self
    }

    /// Boot type to use.
    ///
    /// Possible values: local, bootscript, rescue
    pub fn boot_type(mut self, boot_type: &str) -> ScalewayCreateInstanceBuilder {
        self.config.boot_type = Some(boot_type.to_string());
        self
    }

    /// Bootscript ID to use when boot_type is set to bootscript.
    pub fn bootscript(mut self, bootscript: &str) -> ScalewayCreateInstanceBuilder {
        self.config.bootscript = Some(bootscript.to_string());
        self
    }

    /// True if IPv6 is enabled on the server.
    pub fn enable_ipv6(mut self, enable_ipv6: bool) -> ScalewayCreateInstanceBuilder {
        self.config.enable_ipv6 = Some(enable_ipv6);
        self
    }

    /// ID of the reserved IP to attach to the Instance.
    pub fn public_ip(mut self, public_ip: &str) -> ScalewayCreateInstanceBuilder {
        self.config.public_ip = Some(public_ip.to_string());
        self
    }

    /// Instance Project ID.
    pub fn project(mut self, project: &str) -> ScalewayCreateInstanceBuilder {
        self.config.project = Some(project.to_string());
        self
    }

    /// Tags to apply to the instance
    pub fn tags(mut self, tags: Vec<String>) -> Self {
        self.config.tags = Some(tags);
        self
    }

    /// Security group ID.
    pub fn security_group(mut self, security_group: &str) -> ScalewayCreateInstanceBuilder {
        self.config.security_group = Some(security_group.to_string());
        self
    }

    /// Placement group ID if Instance must be part of a placement group.
    pub fn placement_group(mut self, placement_group: &str) -> ScalewayCreateInstanceBuilder {
        self.config.placement_group = Some(placement_group.to_string());
        self
    }

    #[cfg(feature = "blocking")]
    pub fn run(self) -> Result<ScalewayInstance, ScalewayError> {
        let url = format!(
            "https://api.scaleway.com/instance/v1/zones/{zone}/servers",
            zone = self.zone
        );
        Ok(self
            .api
            .post(&url, self.config)?
            .json::<ScalewayInstanceRoot>()?
            .server)
    }

    pub async fn run_async(self) -> Result<ScalewayInstance, ScalewayError> {
        let url = format!(
            "https://api.scaleway.com/instance/v1/zones/{zone}/servers",
            zone = self.zone
        );
        Ok(self
            .api
            .post_async(&url, self.config)
            .await?
            .json::<ScalewayInstanceRoot>()
            .await?
            .server)
    }
}
