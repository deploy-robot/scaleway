use crate::{
    data::image::{ScalewayImage, ScalewayImageRoot},
    ScalewayApi, ScalewayError,
};

pub struct ScalewayListInstanceImagesBuilder {
    api: ScalewayApi,
    zone: String,
    params: Vec<(&'static str, String)>,
}

impl ScalewayListInstanceImagesBuilder {
    pub fn new(api: ScalewayApi, zone: &str) -> Self {
        ScalewayListInstanceImagesBuilder {
            api,
            zone: zone.to_string(),
            params: vec![],
        }
    }

    /// A positive integer lower or equal to 100 to select the number of items to return.
    pub fn per_page(mut self, count: u32) -> ScalewayListInstanceImagesBuilder {
        self.params.push(("per_page", count.to_string()));
        self
    }

    /// A positive integer to choose the page to return.
    pub fn page(mut self, count: u32) -> ScalewayListInstanceImagesBuilder {
        self.params.push(("page", count.to_string()));
        self
    }

    /// List only images of this Organization ID.
    pub fn organization(mut self, organization: &str) -> ScalewayListInstanceImagesBuilder {
        self.params.push(("organization", organization.to_string()));
        self
    }

    /// List only images of this Project ID.
    pub fn project(mut self, project: &str) -> ScalewayListInstanceImagesBuilder {
        self.params.push(("project", project.to_string()));
        self
    }

    /// Filter images by public attribute
    pub fn public(mut self, public: bool) -> ScalewayListInstanceImagesBuilder {
        self.params.push(("public", public.to_string()));
        self
    }

    /// Filter images by name (eg. "server1" will return "server100" and "server1" but not "foo").
    pub fn name(mut self, name: &str) -> ScalewayListInstanceImagesBuilder {
        self.params.push(("name", name.to_string()));
        self
    }

    /// Filter images by arch
    pub fn arch(mut self, arch: &str) -> ScalewayListInstanceImagesBuilder {
        self.params.push(("arch", arch.to_string()));
        self
    }

    /// List images with these exact tags (to filter with several tags, use commas to separate them).
    pub fn tags(mut self, tags: &str) -> ScalewayListInstanceImagesBuilder {
        self.params.push(("tags", tags.to_string()));
        self
    }

    #[cfg(feature = "blocking")]
    pub fn run(self) -> Result<Vec<ScalewayImage>, ScalewayError> {
        let url = &format!(
            "https://api.scaleway.com/instance/v1/zones/{zone}/images",
            zone = self.zone
        );
        Ok(self
            .api
            .get(&url, self.params)?
            .json::<ScalewayImageRoot>()?
            .images)
    }

    pub async fn run_async(self) -> Result<Vec<ScalewayImage>, ScalewayError> {
        let url = &format!(
            "https://api.scaleway.com/instance/v1/zones/{zone}/images",
            zone = self.zone
        );
        Ok(self
            .api
            .get_async(&url, self.params)
            .await?
            .json::<ScalewayImageRoot>()
            .await?
            .images)
    }
}
