use crate::{
    data::marketplace_local_image::{
        ScalewayMarketplaceLocalImage, ScalewayMarketplaceLocalImagesRoot,
    },
    ScalewayApi, ScalewayError,
};

pub struct ScalewayListMarketplaceLocalImagesBuilder {
    api: ScalewayApi,
    params: Vec<(&'static str, String)>,
}

pub enum LocalImageListType {
    ByImageId(String),
    ByImageLabel(String),
    ByVersionId(String),
}

/// List marketplace local images
///
/// Reference: https://www.scaleway.com/en/developers/api/marketplace/#path-marketplace-local-images-list-local-images-from-a-specific-image-or-version
impl ScalewayListMarketplaceLocalImagesBuilder {
    pub fn new(api: ScalewayApi, list_type: LocalImageListType) -> Self {
        let mut builder = ScalewayListMarketplaceLocalImagesBuilder {
            api,
            params: vec![],
        };
        match list_type {
            LocalImageListType::ByImageId(param) => builder.params.push(("image_id", param)),
            LocalImageListType::ByImageLabel(param) => builder.params.push(("image_label", param)),
            LocalImageListType::ByVersionId(param) => builder.params.push(("version_id", param)),
        };
        builder
    }

    /// Possible Values:
    /// * created_at_asc (Default)
    /// * created_at_desc
    pub fn order_by(mut self, order_by: &str) -> ScalewayListMarketplaceLocalImagesBuilder {
        self.params.push(("order_by", order_by.to_string()));
        self
    }

    pub fn zone(mut self, zone: &str) -> ScalewayListMarketplaceLocalImagesBuilder {
        self.params.push(("zone", zone.to_string()));
        self
    }

    /// Possible Values:
    /// * unknown_type - Unspecified image type (Default)
    /// * instance_local - An image type that can be used to create volumes which are managed via the Instance API.
    /// * instance_sbs - An image type that can be used to create volumes which are managed via the Scaleway Block Storage (SBS) API.
    pub fn image_type(mut self, image_type: &str) -> ScalewayListMarketplaceLocalImagesBuilder {
        self.params.push(("image_type", image_type.to_string()));
        self
    }

    #[cfg(feature = "blocking")]
    pub fn run(self) -> Result<Vec<ScalewayMarketplaceLocalImage>, ScalewayError> {
        let mut list = vec![];
        let mut page = 1;
        loop {
            let mut params = self.params.clone();
            params.push(("page", page.to_string()));
            let result = self
                .api
                .get("https://api.scaleway.com/marketplace/v2/local-images", params)?
                .json::<ScalewayMarketplaceLocalImagesRoot>()?;
            if result.versions.len() == 0 {
                break;
            }
            list.extend(result.versions.into_iter());
            if list.len() == (result.total_count as usize) {
                break;
            }
            page += 1;
        }
        Ok(list)
    }

    pub async fn run_async(self) -> Result<Vec<ScalewayMarketplaceLocalImage>, ScalewayError> {
        let mut list = vec![];
        let mut page = 1;
        loop {
            let mut params = self.params.clone();
            params.push(("page", page.to_string()));
            let result = self
                .api
                .get_async("https://api.scaleway.com/marketplace/v2/local-images", params)
                .await?
                .json::<ScalewayMarketplaceLocalImagesRoot>()
                .await?;
            if result.local_images.len() == 0 {
                break;
            }
            list.extend(result.local_images.into_iter());
            if list.len() == (result.total_count as usize) {
                break;
            }
            page += 1;
        }
        Ok(list)
    }
}
